package com.example.rodri.proyectooscar2;

public class ListItem_Equipos {
    private String id_jugador, nom_equipo, jugador;

    public ListItem_Equipos(String id_jugador, String nom_equipo, String jugador) {
        this.id_jugador = id_jugador;
        this.nom_equipo = nom_equipo;
        this.jugador = jugador;
    }

    public String getId_jugador() {
        return id_jugador;
    }

    public void setId_jugador(String id_jugador) {
        this.id_jugador = id_jugador;
    }

    public String getNom_equipo() {
        return nom_equipo;
    }

    public void setNom_equipo(String nom_equipo) {
        this.nom_equipo = nom_equipo;
    }

    public String getJugador() {
        return jugador;
    }

    public void setJugador(String jugador) {
        this.jugador = jugador;
    }

    @Override
    public String toString() {
        return "Nombre: " + nom_equipo + " Jugador: " + jugador;
    }
}
